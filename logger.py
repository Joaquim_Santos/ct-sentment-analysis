import logging.handlers
import time


def log_setup():
    log_handler = logging.handlers.WatchedFileHandler("sentiment_analysis.log")

    formatter = logging.Formatter('[%(asctime)s] - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    formatter.converter = time.gmtime
    log_handler.setFormatter(formatter)

    logger = logging.getLogger()
    logger.addHandler(log_handler)
    logger.setLevel(logging.INFO)