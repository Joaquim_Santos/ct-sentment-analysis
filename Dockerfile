FROM python:3.6

EXPOSE 5000

WORKDIR /src

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY install_nltk_packages.py build_model.py  data_cleaning.py __init__.py logger.py meus_comentarios.csv run.py  stop_words_git_560.txt  training_test.py ./
COPY static/ static/
COPY templates/ templates/

RUN python install_nltk_packages.py

ENTRYPOINT ["python"]
CMD ["run.py"]