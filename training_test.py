import pandas as pd
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.model_selection import cross_val_predict
from logger import log_setup
import logging


def read_training_input():
    log_setup()
    try:
        dataset = pd.read_csv('meus_comentarios.csv')
        opinions = dataset['Text'].values
        classes = dataset['Classificacao'].values
        return opinions, classes
    except Exception as exception:
        logging.info(f"Ocorreu uma exceção ao ler o arquivo para treino: {exception}")

def build_analysis_model(phrases, classes):
    phrases = numpy.array(phrases)
    classes = numpy.array(classes)
    vectorizer = CountVectorizer(ngram_range=(1, 2))
    frequencies = vectorizer.fit_transform(phrases)
    model = MultinomialNB(alpha=3.0)
    model.fit(frequencies, classes)

    return model, vectorizer, frequencies

def evaluate_performance(model, frequencies, classes):
    result = cross_val_predict(model, frequencies, classes, cv=10)
    accuracy = metrics.accuracy_score(classes, result)
    logging.info(f"A acurácia do mmodelo treinado com a base em questão é {accuracy*100}%\n")

    feelings = ["Positivo", "Negativo", "Neutro"]
    resulting_metrics = metrics.classification_report(classes, result, feelings)
    logging.info(f"As métricas obtidas para o modelo são: {resulting_metrics}")

