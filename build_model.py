import data_cleaning
import training_test
from joblib import dump
import numpy


def build_and_store_model():
    phrases, classes = training_test.read_training_input()
    phrases_wirhout_noise = []
    classes_wirhout_noise = []
    for x in range(0, len(phrases)):
        if (data_cleaning.check_noise(phrases[x])):
            phrases_wirhout_noise.append(phrases[x])
            classes_wirhout_noise.append(classes[x])

    clean_phrases = data_cleaning.clean_text(phrases_wirhout_noise)

    #Queda de 1% de precisão ao aplicar o stemmer
    #clean_phrases = data_cleaning.apply_stemmer(clean_phrases)
    sentiment_model, probability_vector, frequencies = training_test.build_analysis_model(clean_phrases, classes_wirhout_noise)

    classes_wirhout_noise = numpy.array(classes_wirhout_noise)
    training_test.evaluate_performance(sentiment_model, frequencies, classes_wirhout_noise)

    #Salvando Modelos de treinamento criados em disco
    dump(sentiment_model, 'sentiment_model.joblib')
    dump(probability_vector, 'probability_vector.joblib')
    dump(frequencies, 'frequencies.joblib')
