$( document ).ready(function() {
    document.getElementById("comment").value = "";
});

$(function() {
	$('#send-comment').click(function() {
		$("#send-comment").prop("disabled", true);
		document.getElementById("result").innerHTML = "";
		$.ajax({
			url: '/sentiment-analysis',
			data: $('form').serialize(),
			type: 'POST',
			success: function(response) {
				  $('#result').show();
				  var element = document.getElementById("result");

				  if (response["sentment"] =="Positivo"){
						element.innerHTML += "<p>Seu comentário foi classificado  como:</p><p  id=\"sentment\" style=\"color:rgb(0,255,0);\">Positivo - "   +  response["probability"]  +    "</p>";
						 element.innerHTML +=  '<img  id="sentment-result" src="/static/images/positivo.png" > ';
				  }
				  
				  else  if (response["sentment"] =="Negativo"){
						element.innerHTML += "<p>Seu comentário foi classificado  como:</p><p  id=\"sentment\" style=\"color:rgb(255,0,0);\">Negativo - "   +  response["probability"]  +    "</p>";
						 element.innerHTML +=  '<img  id="sentment-result" src="/static/images/negativo.png" > ';
				  }
				  
				  else if (response["sentment"] =="Neutro"){
					  element.innerHTML += "<p>Seu comentário foi classificado  como:</p><p  id=\"sentment\" style=\"color:rgb(28,28,28);\">Neutro - "   +  response["probability"]  +    "</p>";
					   element.innerHTML +=  '<img  id="sentment-result" src="/static/images/neutro.png" > ';
				  }
				   else if (response["sentment"] ==""){
					  element.innerHTML += "<p>Comentário Inválido!</p>";
				  }
				 
				
				$("#send-comment").prop("disabled", false);
				console.log(response);
			},
			error: function(error) {
				alert(error);
			}
		});
	});
});
