import string
import re
import nltk

# nltk.download("all")


stop_words_collecteds = []
with open("stop_words_git_560.txt", "r") as f:
    for word in f.read().split('\n'):
        stop_words_collecteds.append(word)

punctuation_digits = []
for symbol in string.punctuation:
    punctuation_digits.append(symbol)

link_expression = re.compile(
    r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))'''
    , re.DOTALL)

symbol_expression = re.compile(
    r'(@[A-Za-z0-9çÇ]+)|(@)|([A-Za-zçÇ]+[0-9]+[A-Za-zçÇ]+)|([A-Za-zçÇ]+[0-9]+)|([0-9]+[A-Za-zçÇ]+)', re.DOTALL)


def clean_text(phrases):
    clean_phrases = []
    for phrase in phrases:
        clean_phrase = re.sub(link_expression, '', phrase)

        for symbol in punctuation_digits:
            if (symbol != '@' and symbol != '#' and symbol != '!'):
                clean_phrase = clean_phrase.replace(symbol, '')
        for word in clean_phrase.split():
            if word.lower() in stop_words_collecteds:
                clean_phrase = clean_phrase.replace(word, '')

        clean_phrase = re.sub(symbol_expression, '', clean_phrase)

        clean_phrases.append(str(clean_phrase))

    return clean_phrases


def apply_stemmer(phrases):
    stemmer = nltk.stem.RSLPStemmer()  # Stemmer para o português
    phrases_with_stemming = []
    words_with_stemming = ''

    for phrase in phrases:
        for word in phrase.split():
            words_with_stemming = words_with_stemming + str(stemmer.stem(word)) + ' '
        phrases_with_stemming.append(words_with_stemming)
        words_with_stemming = ''

    return phrases_with_stemming


def check_noise(phrase):
    stemmer = nltk.stem.RSLPStemmer()

    if (type(phrase) != str):
        return False
    if (len(phrase.split()) == 0):
        return False

    for word in phrase.split():
        verification = str(stemmer.stem(word))
        if (verification.lower() == 'test'):
            return False

    return True


def clean_phrase(phrase):
    new_phrase = re.sub(link_expression, '', phrase)

    for symbol in punctuation_digits:
        if (symbol != '@' and symbol != '#' and symbol != '!'):
            new_phrase = new_phrase.replace(symbol, '')
    for word in new_phrase.split():
        if word.lower() in stop_words_collecteds:
            new_phrase = new_phrase.replace(word, '')

    new_phrase = re.sub(symbol_expression, '', new_phrase)

    return new_phrase
