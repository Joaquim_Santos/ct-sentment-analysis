from flask import Flask, request, render_template
from joblib import load
import data_cleaning
import build_model
import json


application = Flask(__name__)


@application.route("/sentiment-analysis", methods=["GET","POST"])
def index():
    if request.method == "GET":
        return render_template('index.html')

    try:
        sentiment_model = load('sentiment_model.joblib')
        probability_vector = load('probability_vector.joblib')
    except:
        build_model.build_and_store_model()
        sentiment_model = load('sentiment_model.joblib')
        probability_vector = load('probability_vector.joblib')

    comment = str(request.form['comment'])
    if data_cleaning.check_noise(comment):
        comment = data_cleaning.clean_phrase(comment)
        frequencies = probability_vector.transform([comment])
        probabilities = sentiment_model.predict_proba(frequencies).tolist()[0]  # [Negativo, Neutro, Positivo]
        sentiment = sentiment_model.predict(frequencies).tolist()[0]  # [Negativo, Neutro, Positivo]

        sentment_mapping = {"Positivo": 2, "Neutro": 1, "Negativo": 0}
        selected_prob = sentment_mapping[sentiment]

        probability = format(probabilities[selected_prob] * 100, ".2f") + "%"

        return {"sentment": sentiment, "probability": probability}
    else:
        return {"sentment": ""}


if __name__ == "__main__":
    application.run(host='0.0.0.0')